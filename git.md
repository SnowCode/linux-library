# How to use git
This guide will show you how to use git, from basic to more advanced commands. Take this as a cheatsheet.

## Basic commands

| Purpose | Command |
| ------- | ------- |
| Download a repository | `git clone <url>` |
| Create a repository | `git init` |
| Commit changes | `git add . && git commit -m "<your changes>"` |
| Add a publishing remote | `git remote add origin <url>` |
| Publish committed changes | `git push origin <branch (master)>` |

## Branch management

| Purpose | Command |
| ------- | ------- |
| Create a new branch | `git branch <branch>` |
| List branches | `git branch` | 
| Switch to a branch | `git checkout <branch name>` |
| Remove a branch locally | `git branch -d <branch name>` |
| Push (publish) the deletion of the branch | `git push origin --delete <branch name>` |
| Merge a branch with another | `git checkout <destination> && git merge <source>` |

## Go back in time

| Purpose | Command |
| ------- | ------- |
| Viewing commit history | `git log` |
| Restore a previous commit | `git revert --no-edit <commit>` |
| Cancel individual commits | `git revert --no-edit <commit>..HEAD` |
