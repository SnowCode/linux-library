# How to setup MUTT with Gmail in your terminal
This guide will show you how to setup MUTT with Gmail to manage your emails in your terminal window. Never break your workflow!

1. Install MUTT

```bash
sudo apt-get install -y mutt
```

2. Edit the configuration file

```bash
nano ~/.muttrc
```

3. Paste the following content by changing the <Values>

```
set imap_user = <Your username>@gmail.com
set imap_pass = <Your password>
set smtp_url = "smtp://<Your username>@smtp.gmail.com:567/"
set realname = "<Your name>"
set from = "<Your username>@gmail.com"
set editor = "<your editor (ie: nano)>"

set spoolfile = imaps://imap.gmail.com/INBOX
set folder = imaps://imap.gmail.com/
set record="imaps://imap.gmail.com/[Gmail]/Sent Mail"
set postponed="imaps://imap.gmail.com/[Gmail]/Drafts"
set mbox="imaps://imap.gmail.com/[Gmail]/All Mail"
set header_cache = "~/.mutt/cache/headers"
set message_cachedir = "~/.mutt/cache/bodies"
set certificate_file = "~/.mutt/certificates"
set smtp_pass = $imap_pass
set ssl_force_tls = yes 
set edit_headers = yes
set charset = UTF-8     # value of $LANG; also fallback for send_charset
unset use_domain        
set use_from = yes
```

4. Save the file and run MUTT. 

```bash
mutt
```

5. You can now manage your emails inside your terminal!
