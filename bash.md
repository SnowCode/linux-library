# Learn basic Bash in less than 5 minutes
This is a list of useful Bash commands on Linux and macOS.

> If a command fails for permission issues, type `sudo` in front of the command. 

## Directory management

| Purpose | Command |
| --- | --- | 
| List the files in the current directory | `ls [directory]` |
| Go into a directory | `cd <directory>` |
| Get the current path | `pwd` |
| Get the tree view of the directory | `tree [directory]` |
| Create a new directory | `mkdir <name>` | 
| Remove a directory | `rm -r <directory>` |
| Move or rename a file or a directory | `mv <origin> <destination>` |
| Copy a directory | `cp -r <origin> <destination>` |


## File management

| Purpose | Command |
| --- | --- |
| Download a file | `wget <url>` |
| Create an empty file | `touch <file>` |
| Edit a text file | `nano <file>` |
| Remove a file | `rm <file>` |
| Move or rename a file or a directory | `mv <origin> <destination>` |
| Copy a file | `cp <origin> <destination>` |

### Archives

| Purpose | Command |
| --- | --- |
| Extract a tar archive | `tar xfv <file>` |
| Extract a zip archive | `unzip <file>` |
| Extract a rar archive | `unrar <file>` |

## Software management

| Purpose | Command |
| --- | --- |
| Install a software | `sudo apt-get install <package name>` |
| Remove a software | `sudo apt-get remove <package name>` |
| Install a .DEB package | `sudo apt-get install ./<file>` | 
| Run a file | `chmod +x <file> && ./<file>` |

## System management

| Purpose | Command |
| --- | --- |
| Know if one of your drive is full | `df -h` |
| Know which process is taking most CPU | `top` |
| Get your system details | `neofetch` |
| Power off | `poweroff` |
| Reboot | `reboot` |
| Exit the terminal | `exit` |

## Source code management (Git)
To know how to use git, [click here](./git.md).
