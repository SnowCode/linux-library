# How to customize your terminal
Terminal customization go brrrr.

## Change the colors of your terminal (and the terminal itself)

1. Choose a terminal applicaton (here, tilix)

```bash
sudo apt-get install tilix
```

2. Download schemes and move them to the right directory

```bash
git clone https://codeberg.org/SnowCode/tilix-colors.git
mv tilix-colors/*.json ~/.config/tilix/schemes
rm -r tilix-colors
```

3. Change the tilix settings

```bash
tilix --preferences
```

4. Go into your current profile > Colors > then select the color scheme you want.
5. Restart Tilix

```bash
tilix
```

## Add a powerline instead of the classic thing

> You need to clone this repository to your computer and `cd` into it.

1. Move `.powerline` to your home directory

```bash
mv .powerline ~
```

2. Source it into `.bashrc`

```bash
echo "source ~/.powerline" >> ~/.bashrc
```

3. Source your current terminal window to `~/.bashrc`

```bash
source ~/.bashrc
```

## Use bash-completion to complete things using "tab"
1. Install `bash-completion`

```bash
sudo apt-get install -y bash-completion
```

2. Source it

```bash
echo "source /etc/profile.d/bash_completion.sh" >> ~/.bashrc
source ~/.bashrc
```

## Use aliases
1. Create a alias file

```bash
nano ~/.bash_aliases
```

2. Write your aliases like this:

```bash
alias palias='nano ~/.bash_aliases && source ~/.bashrc'
alias mkdir='mkdir -vp'
alias cp='cp -riv'
alias mv='mv -iv'
alias rm='rm -iv'
alias gic='git add . && git commit -m'
alias gip='git push origin master'
```

3. Source it

```bash
echo "source ~/.bash_aliases" >> ~/.bashrc
source .bashrc
```
