# Linux git wiki
This is a wiki using Git about anything you want related to Linux and open-source.

## Advantages

* Distributed, anybody can host the wiki, download the content, edit and view the content offline, etc.
* More options than a normal wiki because anyfile can be hsoted here
* Possibility to create different branches for different wikis
* Can be done integrally in cmd

## How to use the wiki (terminal)

1. Fork the repository
2. Clone your repository

```bash
git clone https://codeberg.org/<username>/linux-library.git
cd linux-library
ls
```

2. Read a file

```bash
nano <the file you want to read>
ls
```

3. Edit a file

```bash
nano <the file you want to read>
git add .
git commit -m "<description of your changes>"
git push origin master
```

4. Make a merge request on the website

